# KFC Digital Edge Helm Chart

This project contains a Helm chart to deploy the various KFC Edge applications and services onto the in-restaurant Kubernetes cluster.

This project is inclusive of the following applications and services:

- MongoDB
- RabbitMQ
- Edge Employee API
- Edge Order API
- Edge Pricing API
- Edge Kiosk API
- Edge Message Router
- Dragontail

This Helm chart is setup so it doesn't not require any dependencies, and will install the Edge applications and services without needing any other dependencies.

Secrets - like credential passwords and security tokens - are generated if they don't already exist or the chart will re-use values that have already been assigned.

Namespaces have been introduced to partition applications and services, and their dependencies, as this prevents name collisions and adds another layer to separate secrets.

## Versioning

The Helm Chart has two version attributes - `version` and `appVersion`. As this chart is for a bundle of applications and services, both of the `version` and `appVersion` attributes should be assigned the same value.

Versioning ashould following the format `<fiscal year>.<fiscal period>.<release iteration>`, where:

- `<fiscal year>` is the 4 digit KFC fiscal year this release was built in
- `<fiscal period>` is the 2 digit KFC fiscal period this release was built in
- `<release iteration>` is an accummulated release calendar that should be incremented for each release with the fiscal year and period

When a new release is built for QA, UAT or PROD, the version should be updated and a correspnding repo branch should be created - like `release/2023.05.1` - so we can later reference application and service versions in the release.

## Main Settings

The following settings are considered the most important for customizing the installation for specific environments. Values for these setting should be provided by the host system and overridden when calling `helm` commands.

| Setting                           | Required | Description                                                                                                                                                                                                                  |
| --------------------------------- | -------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| imagePullSecret.secretValueBase64 | Yes      | A base-64 encoded GitLab auth JSON payload. This value will be used to authenticate against KFC's GitLab instance when pulling private container images.                                                                     |
| clusterEnvironment                | No       | Defaults to "PROD" if not provided. This setting allows for setting environment-specific values within the Helm chart templates. Allowed values are: "DEV", "QA", "UAT" and "PROD".                                          |
| useIngressHostnames               | No       | Defaults to "true" if not provided. Sets whether HTTP ingress is limited to a specific host header or accepts traffic from any host header.                                                                                  |
| runMultiMode                      | No       | Defaults to "true" if not provided. Sets whether supporting application will be run with multiple containers. This option is provided to reduce resources in development environments and for lower-powered QA/lab installs. |
| restaurant.restaurantId           | Yes      | The Restaurant ID for the installation. This is used by the various applications.                                                                                                                                            |
| restaurant.swaggerEnabled         | No       | Defaults to "false" if not provided. This setting is used by the various .NET applications and controls whether SwaggerDoc is accessible within the application. Allowed values are "true" and "false".                      |
| restaurant.timeZone               | Yes      | A TZ Identifier that is used to set the time zone for the running containers, like "America/Kentucky/Louisville".                                                                                                            |
| restaurant.certPemBase64          | Yes      | A base-64 encoded value of the "store cert" PEM. This is used to authenticate against Cognito for the cloud services that require it.                                                                                        |
| metalLBIp                         | No       | The IP address used to hit the MetalLB load balancer. Not required for development testing. This is used by Kiosk Services as the URL replacement host when no explicit value is provided.

**Note:** Additional important configuration values related to each service can be found in the following sections.

## Local Development and Testing

[Rancher Desktop](https://www.rancher.com/products/rancher-desktop) is the preferred way to run these services locally. Once Rancher Desktop is installed, you'll be asked to set it up. Please use the following:

- Ensure that Enable Kubernetes is checked
- Ensure that the latest, stable version of Kubernetes is selected
- Ensure that containerd is selected
- Ensure that Configure Path is set to automatic

Once Rancher Desktop is installed, you'll need to:

```bash
cp .env-sample .env
cp localvalues-sample-dev.yaml localvalues.yaml
```

Then edit the `.env` file and `localvalues.yaml` and make any necessary changes, following the instructions inside the file. Most changes should be made in `localvalues.yaml`, copying from `values.yaml`.

The `.env` file contains only values that need to be computed live on your system, and can be quite light, reducing to:

```.env
KFC_ID=DXD7510
GITLAB_TOKEN="TOKENVALUE"
export HELM_IMAGE_PULL_SECRET_BASE64=$(echo -n '{"auths":{"registry.gitlab.yum.com":{"username":"'"$KFC_ID"'","password":"'"$GITLAB_TOKEN"'"}}}' | base64 | tr -d '\n')
export RESTAURANT_CERT_PEM_BASE64=$(base64 -i ~/Projects/kfc/pem/Z123456.crt.pem)
```

If you plan on running Snyk scans locally, you'll need the Snyk CLI installed and accessble in your path envvar. https://docs.snyk.io/snyk-cli/install-or-update-the-snyk-cli

You can use the `local-dev-setup.sh` script to lint, verify and install/upgrade a `kfc-digital-edge-common` namespace containing restaurant-specific settings that are inherited by `kfc-digital-edge`.

You can use the `local-dev.sh` script to lint, verify and install/upgrade the `kfc-digital-edge` which contains the KFC Digital Edge applications and services. The `kfc-digital-edge-common` should exist before running this step!

The following options can be used to verify that the templates and values are correct.

```bash
# Run lint on the Helm charts
bash local-dev.sh lint
# Run a local Snyk IaC test, with a severity threshold of medium
bash local-dev.sh snyk
# Run a local Snyk IaC test, without a severity threshold
bash local-dev.sh snyk-all-issues
# Do a dry run to verify templates are rendered and the correct values used
bash local-dev.sh dry-run
# Do a dry run to verify templates are rendered and the correct values used
# and print out extra debugging if something is wrong
bash local-dev.sh dry-run-debug
```

To install or upgrade the KFC Digital Edge applications and services, run (in this order):

```bash
bash local-dev.sh upgrade
```

To monitor the pods while upgrading/running/deleting:

```bash
watch 'kubectl get pods -A | sort | grep -v "kube-system"'
```

## Testing Websockets for the Message Router

The `kfc-digital-message-router-service` has support for websockets and websockets are used to receive asynchronous messages from Poseidon.

[Instructions on setting up websockets on Poseidon](https://yumbrands.atlassian.net/wiki/spaces/YPOS/pages/1792999825/How+To+Set+Up+Poseidon+For+Template+and+or+KFC#Setting-up-External-Link-Service) to push updates to the `kfc-digital-message-router-service`.

The websocket URL should be either:

- `ws://servicestation.kfcdev.io/message-router/ws`, if ingress hostnames **are** used
- `ws://<node external ip>/message-router/ws`, if ingress hostnames **are not** used

An alternative method of testing to using Poseidon, you can use [websocat](https://github.com/vi/websocat/tree/master) for local testing.

## Running and Testing Kiosk Services
The `kfc-digital-kiosk-services` has a number of configuration values, the most important of which are described below:

| Setting                                                                        | Required | Description                                                                                                                                                                                                                                                                                                                                                               |
|--------------------------------------------------------------------------------|----------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| kioskSecrets.kfcDigitalAuthCognitoClientId                                     | Yes      | Client ID for Cognito credentials used to authenticate with KFC Digital cloud services (restaurant doc & receipts).                                                                                                                                                                                                                                                       |
| kioskSecrets.kfcDigitalAuthCognitoClientSecret                                 | Yes      | Client Secret for Cognito credentials used to authenticate with KFC Digital cloud services (restaurant doc & receipts).                                                                                                                                                                                                                                                   |
| kioskSecrets.yumMenuAuthCognitoClientId                                        | Yes      | Client ID for Cognito credentials used to authenticate with Yum's GraphQL API (restaurant information and YumV2 menu).                                                                                                                                                                                                                                                    |
| kioskSecrets.yumMenuAuthCognitoClientSecret                                    | Yes      | Client Secret for Cognito credentials used to authenticate with Yum's GraphQL API (restaurant information and YumV2 menu).                                                                                                                                                                                                                                                |
| kioskServices.envs.{qa\|prod}.urlReplacementConfiguration.serviceHost          | No       | The host name used to access this service. It is used to replace URLs found in files such as the Privacy & Terms documents as well as the YumV2 menu. If it is not found, the value equivalent to `http://{{METALLB_IP}}/kiosk-api` will be used instead. `METALLB_IP` is provided by the `restaurant-config` ConfigMap found in the `kfc-digital-edge-common` namespace. |
| kioskServices.envs.{qa\|prod}.poseidonOrderConfiguration.posKioskOrderEndpoint | No       | The endpoint used to access the Poseidon (POS) APIs. If this is not present, Kiosk Services will use the `PoseidonInfo` message to determine the primary POS IP address and port, then send orders to a composed endpoint.                                                                                                                                                |

## Ignoring Snyk Issue Vulnerabilities for the Pipeline Job

There can be cases when KFC would like to ignore one or more Snyk Issue Vulnerabilities that are identified during the Snyk IaC test.

For example, the following issue identifed a permission vulnerability but we need to ignore it because we use init containers to set up permissions and configuration.

```bash
  [Medium] Container is running without privilege escalation control
  Info:    `allowPrivilegeEscalation` attribute is not set to `false`. Processes
           could elevate current privileges via known vectors, for example SUID
           binaries
  Rule:    https://security.snyk.io/rules/cloud/SNYK-CC-K8S-9
  Path:    [DocId: 6] > spec > template > spec > initContainers[dt-init] >
           securityContext > allowPrivilegeEscalation
  File:    kfc-digital-edge/templates/kfc-dragontail-service.yaml
  Resolve: Set `spec.{containers,
           initContainers}.securityContext.allowPrivilegeEscalation` to `false`
```

To ignore this issue, the following command was run and the ignore rule was added to the `.snyk` file:

```bash
snyk ignore --id=SNYK-CC-K8S-9 --path="kfc-digital-edge/templates/kfc-dragontail-service.yaml > [DocId: 6] > spec > template > spec > initContainers[dt-init] > securityContext > allowPrivilegeEscalation" --expiry=2666-01-02 --reason="Configuration script requires root"
```

As ignore rules should be as specific as possible, the `--path` is comprised of the `File` and `Path` values from the identified vulnerability.

An expiry date of well into the future was also provided as this is unlikely to change. If the `--expiry` value is not provided, the ignore rule defaults to 30 days.

You should also provide a meaningful reason.

## DataDog .NET Tracer compatibility

Regarding DataDog's support for for .NET Core, the following pages should be used to verify that the `datadog.dotnetLibVersion` values.yaml attribute in the `kfc-digital-edge` helm chart is compatible.

- https://docs.datadoghq.com/tracing/trace_collection/compatibility/dotnet-core/
- https://github.com/DataDog/dd-trace-dotnet
