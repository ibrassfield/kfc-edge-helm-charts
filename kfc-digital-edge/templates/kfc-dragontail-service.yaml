{{- if eq .Values.dragontail.useDragontail true }}
---
{{- include "imagePullSecretTemplate" (list . "kfc-dragontail-service") }}

{{- $restaurantConfig := (lookup "v1" "ConfigMap" "kfc-digital-edge-common" "restaurant-config") }}
{{- $restaurantConfigData := (get $restaurantConfig "data") | default dict }}
{{- $restaurantConfigRestaurantId := (get $restaurantConfigData "RESTAURANT_ID") }}
{{- $restaurantConfigClusterEnvironment := (get $restaurantConfigData "CLUSTER_ENVIRONMENT") }}

{{- $dragontailAWSAccessKeySecret := (lookup "v1" "Secret" "kfc-digital-edge-common" "kfc-dragontail-service-aws-access-key-secret") }}
{{- $dragontailAWSAccessKeySecretData := (get $dragontailAWSAccessKeySecret "data") | default dict }}
{{- $dragontailAWSAccessKeyId := (get $dragontailAWSAccessKeySecretData "AWS_ACCESS_KEY_ID") }}
{{- $dragontailAWSAccessKeyValue := (get $dragontailAWSAccessKeySecretData "AWS_SECRET_ACCESS_KEY") }}
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: dragontail-ingress-8080-kfcdev
  namespace: kfc-dragontail-service
spec:
  entryPoints:
    - dt8080
  routes:
    - kind: Rule
{{- if eq .Values.useIngressHostnames true }}
      match: Host(`dt.kfcdev.io`)
{{- else }}
      match: HostRegexp(`{any:.+}`)
{{- end }}
      services:
        - name: kfc-dragontail-service-dt8080
          port: 8080
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: dragontail-ingress-8086-kfcdev
  namespace: kfc-dragontail-service
spec:
  entryPoints:
    - dt8086
  routes:
    - kind: Rule
{{- if eq .Values.useIngressHostnames true }}
      match: Host(`dt.kfcdev.io`)
{{- else }}
      match: HostRegexp(`{any:.+}`)
{{- end }}
      services:
        - name: kfc-dragontail-service-dt8086
          port: 8086
---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: dragontail-ingress-8192-kfcdev
  namespace: kfc-dragontail-service
spec:
  entryPoints:
    - dt8192
  routes:
    - kind: Rule
{{- if eq .Values.useIngressHostnames true }}
      match: Host(`dt.kfcdev.io`)
{{- else }}
      match: HostRegexp(`{any:.+}`)
{{- end }}
      services:
        - name: kfc-dragontail-service-dt8192
          port: 8192
---
apiVersion: v1
kind: Service
metadata:
  name: kfc-dragontail-service-dt8192
  namespace: kfc-dragontail-service
spec:
  selector:
    app: kfc-dragontail-service
  ports:
  - port: 8192
    targetPort: 8192
  type: ClusterIP
---
apiVersion: v1
kind: Service
metadata:
  name: kfc-dragontail-service-dt8080
  namespace: kfc-dragontail-service
spec:
  selector:
    app: kfc-dragontail-service
  ports:
  - port: 8080
    targetPort: 8080
  type: ClusterIP
---
apiVersion: v1
kind: Service
metadata:
  name: kfc-dragontail-service-dt8086
  namespace: kfc-dragontail-service
spec:
  selector:
    app: kfc-dragontail-service
  ports:
  - port: 8086
    targetPort: 8086
  type: ClusterIP
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: kfc-dragontail-service-pvc
  namespace: kfc-dragontail-service
spec:
  accessModes:
  - ReadWriteOnce
  {{- if eq .Values.useLonghorn true }}
  storageClassName: longhorn
  {{- else }}
  storageClassName: local-path
  {{- end}}
  resources:
    requests:
      storage: 20Gi
  volumeMode: Filesystem
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: kfc-dragontail-service
  namespace: kfc-dragontail-service
spec:
  selector:
    matchLabels:
      app: kfc-dragontail-service
  strategy:
    type: Recreate
  template:
    metadata:
      annotations:
        checksum/secret: {{ printf "%s-%s-%s" (include "imagePullSecretTemplate" (list . "kfc-digital-employee-services")) $dragontailAWSAccessKeyId $dragontailAWSAccessKeyValue | sha256sum }}
      labels:
        app: kfc-dragontail-service
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 100
              podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - kfc-dragontail-service
                topologyKey: kubernetes.io/hostname
      securityContext:
        seccompProfile:
          type: RuntimeDefault
      initContainers:
      - name: dt-init
        image: {{ .Values.alpine.image }}:{{ .Values.alpine.imageTag }}
        imagePullPolicy: IfNotPresent
        command: ["sh", "-c", "chown -R 1000:1000 /opt/dragontail"]
        volumeMounts:
          - name: kfc-dragontail-service-volume
            mountPath: /opt/dragontail
      containers:
      - name: dt
        image: {{ .Values.dragontail.image }}:{{ .Values.dragontail.imageTag }}
        imagePullPolicy: IfNotPresent
        securityContext:
          allowPrivilegeEscalation: false
          runAsNonRoot: true
          runAsUser: 1000
          runAsGroup: 1000
          capabilities:
            drop:
              - ALL
        stdin: true
        ports:
        - containerPort: 8192
        - containerPort: 8080
        - containerPort: 8086
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        tty: true
        envFrom:
          - secretRef:
              name: kfc-dragontail-service-aws-access-key-secret
        env:
          - name: Customer
            value: KFC-US
          - name: Store
            value: {{ $restaurantConfigRestaurantId }}
          - name: StoreConf
            value: {{ .Values.dragontail.storeConfigVersion }}
          {{- if eq ($restaurantConfigClusterEnvironment) "PROD" }}
          - name: Env
            value: {{ .Values.dragontail.envs.production.environmentName }}
          {{- else }}
          - name: Env
            value: {{ .Values.dragontail.envs.qa.environmentName }}
          {{- end }}
        volumeMounts:
          - name: kfc-dragontail-service-volume
            mountPath: /opt/dragontail
      imagePullSecrets:
        - name: {{ .Values.imagePullSecret.secretName }}
      terminationGracePeriodSeconds: 10
{{- include "tolerationsForNodeEviction" . | indent 6 }}
      volumes:
        - name: kfc-dragontail-service-volume
          persistentVolumeClaim:
            claimName: kfc-dragontail-service-pvc
---
apiVersion: v1
kind: Secret
metadata:
  name: kfc-dragontail-service-aws-access-key-secret
  namespace: kfc-dragontail-service
type: Opaque
data:
  AWS_ACCESS_KEY_ID: {{ $dragontailAWSAccessKeyId }}
  AWS_SECRET_ACCESS_KEY: {{ $dragontailAWSAccessKeyValue }}
{{- end }}