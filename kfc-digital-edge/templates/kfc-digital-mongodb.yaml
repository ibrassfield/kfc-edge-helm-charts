{{- $restaurantConfig := (lookup "v1" "ConfigMap" "kfc-digital-edge-common" "restaurant-config") }}
{{- $restaurantConfigData := (get $restaurantConfig "data") | default dict }}
{{- $restaurantConfigRestaurantId := (get $restaurantConfigData "RESTAURANT_ID") }}
{{- $restaurantConfigSwaggerEnabled := (get $restaurantConfigData "APP_SWAGGER_ENABLED") | quote }}
{{- $restaurantConfigTimezone := (get $restaurantConfigData "TIME_ZONE") }}
{{- $restaurantConfigRunMultiNode := (get $restaurantConfigData "RUN_MULTI_MODE") }}
{{- $restaurantConfigClusterEnvironment := (get $restaurantConfigData "CLUSTER_ENVIRONMENT") }}

{{- if ne ($restaurantConfigClusterEnvironment) "MOCK" }}

{{- include "imagePullSecretTemplate" (list . "kfc-digital-mongodb") }}

{{- $mongoDbKeyfileSecret := (lookup "v1" "Secret" "kfc-digital-mongodb" "kfc-digital-mongodb-keyfile-secret") }}
{{- $mongoDbKeyfileSecretData := (get $mongoDbKeyfileSecret "data") | default dict }}
{{- $mongoDbKeyfile := (get $mongoDbKeyfileSecretData "mongodb-keyfile") | default (randAlphaNum 741 | b64enc) }}

{{- $mongoDbSecret := (lookup "v1" "Secret" "kfc-digital-mongodb" "kfc-digital-mongodb-secret") }}
{{- $mongoDbSecretData := (get $mongoDbSecret "data") | default dict }}
{{- $mongoDbUsersAdminPassword := (get $mongoDbSecretData "CREDENTIALCONFIGURATION__ADMINPASSWORD") | default (randAlphaNum 20 | b64enc) }}
{{- $mongoDbUsersEmployeeApiPassword := (get $mongoDbSecretData "CREDENTIALCONFIGURATION__EMPLOYEEAPIPASSWORD") | default (randAlphaNum 20 | b64enc) }}
{{- $mongoDbUsersKioskApiPassword := (get $mongoDbSecretData "CREDENTIALCONFIGURATION__KIOSKAPIPASSWORD") | default (randAlphaNum 20 | b64enc) }}
{{- $mongoDbUsersPricingApiPassword := (get $mongoDbSecretData "CREDENTIALCONFIGURATION__PRICINGAPIPASSWORD") | default (randAlphaNum 20 | b64enc) }}
{{- $mongoDbUsersMessageRouterPassword := (get $mongoDbSecretData "CREDENTIALCONFIGURATION__MESSAGEROUTERPASSWORD") | default (randAlphaNum 20 | b64enc) }}
{{- $mongoDbUsersMetricsPassword := (get $mongoDbSecretData "CREDENTIALCONFIGURATION__METRICSPASSWORD") | default (randAlphaNum 20 | b64enc) }}

---
apiVersion: v1
kind: Secret
metadata:
  name: kfc-digital-mongodb-keyfile-secret
  namespace: kfc-digital-mongodb
type: Opaque
data:
  mongodb-keyfile: {{ $mongoDbKeyfile }}

---
apiVersion: v1
kind: Secret
metadata:
  name: kfc-digital-mongodb-secret
  namespace: kfc-digital-mongodb
type: Opaque
data:
  CREDENTIALCONFIGURATION__ADMINUSERNAME: {{ .Values.mongodb.users.adminUsername | b64enc }}
  CREDENTIALCONFIGURATION__ADMINPASSWORD: {{ $mongoDbUsersAdminPassword }}
  CREDENTIALCONFIGURATION__EMPLOYEEAPIUSERNAME: {{ .Values.mongodb.users.employeeApiUsername | b64enc }}
  CREDENTIALCONFIGURATION__EMPLOYEEAPIPASSWORD: {{ $mongoDbUsersEmployeeApiPassword }}
  CREDENTIALCONFIGURATION__KIOSKAPIUSERNAME: {{ .Values.mongodb.users.kioskApiUsername | b64enc }}
  CREDENTIALCONFIGURATION__KIOSKAPIPASSWORD: {{ $mongoDbUsersKioskApiPassword }}
  CREDENTIALCONFIGURATION__PRICINGAPIUSERNAME: {{ .Values.mongodb.users.pricingApiUsername | b64enc }}
  CREDENTIALCONFIGURATION__PRICINGAPIPASSWORD: {{ $mongoDbUsersPricingApiPassword }}
  CREDENTIALCONFIGURATION__MESSAGEROUTERUSERNAME: {{ .Values.mongodb.users.messageRouterUsername | b64enc }}
  CREDENTIALCONFIGURATION__MESSAGEROUTERPASSWORD: {{ $mongoDbUsersMessageRouterPassword }}
  CREDENTIALCONFIGURATION__METRICSUSERNAME: {{ .Values.mongodb.users.metricsUsername | b64enc }}
  CREDENTIALCONFIGURATION__METRICSPASSWORD: {{ $mongoDbUsersMetricsPassword }}

---
apiVersion: v1
kind: Secret
metadata:
  name: kfc-digital-employee-services-mongodb-secret
  namespace: kfc-digital-employee-services
type: Opaque
data:
  MONGODBCONFIGURATION__USERNAME: {{ .Values.mongodb.users.employeeApiUsername | b64enc }}
  MONGODBCONFIGURATION__PASSWORD: {{ $mongoDbUsersEmployeeApiPassword }}

---
apiVersion: v1
kind: Secret
metadata:
  name: kfc-digital-kiosk-services-mongodb-secret
  namespace: kfc-digital-kiosk-services
type: Opaque
data:
  MONGODBCONFIGURATION__USERNAME: {{ .Values.mongodb.users.kioskApiUsername | b64enc }}
  MONGODBCONFIGURATION__PASSWORD: {{ $mongoDbUsersKioskApiPassword }}

---
apiVersion: v1
kind: Secret
metadata:
  name: kfc-digital-pricing-services-mongodb-secret
  namespace: kfc-digital-pricing-services
type: Opaque
data:
  MONGODBCONFIGURATION__USERNAME: {{ .Values.mongodb.users.pricingApiUsername | b64enc }}
  MONGODBCONFIGURATION__PASSWORD: {{ $mongoDbUsersPricingApiPassword }}

---
apiVersion: v1
kind: Secret
metadata:
  name: kfc-digital-message-router-services-mongodb-secret
  namespace: kfc-digital-message-router-services
type: Opaque
data:
  MONGODBCONFIGURATION__USERNAME: {{ .Values.mongodb.users.messageRouterUsername | b64enc }}
  MONGODBCONFIGURATION__PASSWORD: {{ $mongoDbUsersMessageRouterPassword }}

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: restaurant-config
  namespace: kfc-digital-mongodb
data:
  RESTAURANT_ID: {{ $restaurantConfigRestaurantId }}
  SWAGGER_ENABLED: {{ $restaurantConfigSwaggerEnabled }}
  TIME_ZONE: {{ ($restaurantConfigTimezone) }}

---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: kfc-digital-mongodb-watcher-role
  namespace: kfc-digital-mongodb
rules:
  - apiGroups: [""]
    resources: ["pods"]
    verbs: ["get", "list", "watch"]
  - apiGroups: [""]
    resources: ["pods/exec"]
    verbs: ["get"]

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: kfc-digital-mongodb-watcher-service-account
  namespace: kfc-digital-mongodb

---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: kfc-digital-mongodb-watcher-role-binding
  namespace: kfc-digital-mongodb
subjects:
  - kind: ServiceAccount
    name: kfc-digital-mongodb-watcher-service-account
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: kfc-digital-mongodb-watcher-role

---
apiVersion: v1
kind: Service
metadata:
  name: kfc-digital-mongodb-watcher-services
  namespace: kfc-digital-mongodb
spec:
  type: ClusterIP
  sessionAffinity: None
  ports:
    - port: 5208
      name: kfc-emsrv
      protocol: TCP
      targetPort: 5208
  selector:
    app: kfc-digital-mongodb-watcher-services

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: servicestation
  namespace: kfc-digital-mongodb
spec:
  entryPoints:
    - web
  routes:
    - kind: Rule
{{- if eq .Values.useIngressHostnames true }}
      match: Host(`servicestation.kfcdev.io`) && PathPrefix(`/mongodb-watcher`)
{{- else }}
      match: HostRegexp(`{any:.+}`) && PathPrefix(`/mongodb-watcher`)
{{- end }}
      services:
        - name: kfc-digital-mongodb-watcher-services
          port: 5208

---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: kfc-digital-mongodb-watcher-services
    tags.datadoghq.com/env: {{ $restaurantConfigClusterEnvironment }}
    tags.datadoghq.com/service: {{ index "kfc-digital-mongodb-watcher-services-RESTAURANTID" | replace "RESTAURANTID" $restaurantConfigRestaurantId }}
    tags.datadoghq.com/version: {{ .Chart.Version }}
  name: kfc-digital-mongodb-watcher-services
  namespace: kfc-digital-mongodb
spec:
  replicas: 1
  selector:
    matchLabels:
      app: kfc-digital-mongodb-watcher-services
  template:
    metadata:
      annotations:
        checksum/secret: {{ (include "imagePullSecretTemplate" (list . "kfc-digital-mongodb")) | sha256sum }}
        admission.datadoghq.com/dotnet-lib.version: {{ .Values.datadog.dotnetLibVersion }}
      labels:
        app: kfc-digital-mongodb-watcher-services
        admission.datadoghq.com/enabled: 'true'
        tags.datadoghq.com/env: {{ $restaurantConfigClusterEnvironment }}
        tags.datadoghq.com/service: {{ index "kfc-digital-mongodb-watcher-services-RESTAURANTID" | replace "RESTAURANTID" $restaurantConfigRestaurantId }}
        tags.datadoghq.com/version: {{ .Chart.Version }}
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 100
              podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - kfc-digital-mongodb-watcher-services
                topologyKey: kubernetes.io/hostname
      serviceAccountName: kfc-digital-mongodb-watcher-service-account
      securityContext:
        seccompProfile:
          type: RuntimeDefault
      containers:
        - name: kfc-digital-mongodb-watcher-services
          image: {{ .Values.mongodb.watcherServices.image }}:{{ .Values.mongodb.watcherServices.imageTag }}
          imagePullPolicy: IfNotPresent
          resources:
            limits:
              cpu: {{ .Values.mongodb.watcherServices.resourcesLimitsCpu }}
              memory: {{ .Values.mongodb.watcherServices.resourcesLimitsMemory }}
            requests:
              cpu: {{ .Values.mongodb.watcherServices.resourcesRequestsCpu }}
              memory: {{ .Values.mongodb.watcherServices.resourcesRequestsMemory}}
          securityContext:
            allowPrivilegeEscalation: false
            runAsNonRoot: true
            runAsUser: 10008
            runAsGroup: 10008
            capabilities:
              drop:
                - ALL
          ports:
            - containerPort: 5208
          startupProbe:
            httpGet:
              port: 5208
              path: /v1/health/readiness-check
            initialDelaySeconds: 30
            periodSeconds: 10
          livenessProbe:
            httpGet:
              port: 5208
              path: /v1/health/liveness-check
            failureThreshold: 1
          envFrom:
            - secretRef:
                name: kfc-digital-mongodb-secret
          env:
            - name: ASPNETCORE_ENVIRONMENT
              value: "Production"
            - name: ASPNETCORE_URLS
              value: "http://0.0.0.0:5208"
            - name: APPLICATIONFEATURES__SWAGGERENABLED
              valueFrom:
                configMapKeyRef:
                  name: restaurant-config
                  key: SWAGGER_ENABLED
            - name: RESTAURANTCONFIGURATION__RESTAURANTID
              valueFrom:
                configMapKeyRef:
                  name: restaurant-config
                  key: RESTAURANT_ID
            - name: TZ
              valueFrom:
                configMapKeyRef:
                  name: restaurant-config
                  key: TIME_ZONE
            {{- if eq ($restaurantConfigRunMultiNode) "true" }}
            - name: MONGOCLUSTERCONFIGURATION__URL
              value: "mongodb://kfc-digital-mongodb-0.kfc-digital-mongodb:27017,kfc-digital-mongodb-1.kfc-digital-mongodb:27017,kfc-digital-mongodb-2.kfc-digital-mongodb:27017/admin?w=majority&replicaSet=rs0"
            - name: MONGOCLUSTERCONFIGURATION__ISMULTINODE
              value: "true"
            {{- else }}
            - name: MONGOCLUSTERCONFIGURATION__URL
              value: "mongodb://kfc-digital-mongodb-0.kfc-digital-mongodb:27017/admin?w=majority"
            - name: MONGOCLUSTERCONFIGURATION__ISMULTINODE
              value: "false"
            {{- end }}
            - name: DD_LOGS_INJECTION
              value: 'true'
      imagePullSecrets:
        - name: {{ .Values.imagePullSecret.secretName }}
      terminationGracePeriodSeconds: 10
{{- include "tolerationsForNodeEviction" . | indent 6 }}

---
kind: Service
apiVersion: v1
metadata:
  name: kfc-digital-mongodb-0
  namespace: kfc-digital-mongodb
spec:
  type: ClusterIP
  clusterIP: None
  ports:
    - name: mongodb-srv
      protocol: TCP
      port: 27017
      targetPort: 27017
  selector:
    statefulset.kubernetes.io/pod-name: kfc-digital-mongodb-0

{{- if eq ($restaurantConfigRunMultiNode) "true" }}
---
kind: Service
apiVersion: v1
metadata:
  name: kfc-digital-mongodb-1
  namespace: kfc-digital-mongodb
spec:
  type: ClusterIP
  clusterIP: None
  ports:
    - name: mongodb-srv
      protocol: TCP
      port: 27017
  selector:
    statefulset.kubernetes.io/pod-name: kfc-digital-mongodb-1

---
kind: Service
apiVersion: v1
metadata:
  name: kfc-digital-mongodb-2
  namespace: kfc-digital-mongodb
spec:
  type: ClusterIP
  clusterIP: None
  ports:
    - name: mongodb-srv
      protocol: TCP
      port: 27017
  selector:
    statefulset.kubernetes.io/pod-name: kfc-digital-mongodb-2
{{- end }}

---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  labels:
    tags.datadoghq.com/env: {{ $restaurantConfigClusterEnvironment }}
    tags.datadoghq.com/service: {{ index "kfc-digital-mongodb-RESTAURANTID" | replace "RESTAURANTID" $restaurantConfigRestaurantId }}
    tags.datadoghq.com/version: {{ .Chart.Version }}
  name: kfc-digital-mongodb
  namespace: kfc-digital-mongodb
spec:
  serviceName: mongodb
  {{- if eq ($restaurantConfigRunMultiNode) "true" }}
  replicas: 3
  {{- else }}
  replicas: 1
  {{- end }}
  selector:
    matchLabels:
      app: kfc-digital-mongodb
  template:
    metadata:
      annotations:
        ad.datadoghq.com/kfc-digital-mongodb.checks: {{- .Values.datadog.mongodbConfig | replace "METRICSPASSWORD" ($mongoDbUsersMetricsPassword | b64dec) | toYaml | indent 1 }}
      labels:
        app: kfc-digital-mongodb
        admission.datadoghq.com/enabled: 'true'
        tags.datadoghq.com/env: {{ $restaurantConfigClusterEnvironment }}
        tags.datadoghq.com/service: {{ index "kfc-digital-mongodb-RESTAURANTID" | replace "RESTAURANTID" $restaurantConfigRestaurantId }}
        tags.datadoghq.com/version: {{ .Chart.Version }}
    spec:
      affinity:
        podAffinity: {}
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 100
              podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                        - kfc-digital-mongodb
                topologyKey: kubernetes.io/hostname
      securityContext:
        seccompProfile:
          type: RuntimeDefault
      initContainers:
        - name: kfc-digital-mongodb-init
          image: {{ .Values.alpine.image }}:{{ .Values.alpine.imageTag }}
          imagePullPolicy: IfNotPresent
          command: ['sh', '-c', 'cp /secret/* /data/key/ && chown 10001:10001 /data/key/mongodb-keyfile']
          volumeMounts:
            - name: secret
              mountPath: /secret
            - name: keyfile
              mountPath: /data/key
      containers:
        - name: kfc-digital-mongodb
          image: {{ .Values.mongodb.service.image }}:{{ .Values.mongodb.service.imageTag }}
          imagePullPolicy: IfNotPresent
          resources:
            limits:
              cpu: {{ .Values.mongodb.service.resourcesLimitsCpu }}
              memory: {{ .Values.mongodb.service.resourcesLimitsMemory }}
            requests:
              cpu: {{ .Values.mongodb.service.resourcesRequestsCpu }}
              memory: {{ .Values.mongodb.service.resourcesRequestsMemory}}
          securityContext:
            allowPrivilegeEscalation: false
            runAsNonRoot: true
            runAsUser: 10001
            runAsGroup: 10001
            capabilities:
              drop:
                - ALL
          env:
            - name: TZ
              valueFrom:
                configMapKeyRef:
                  name: restaurant-config
                  key: TIME_ZONE
            - name: DD_LOGS_INJECTION
              value: 'true'
          command: 
            - mongod
          args:
            - --replSet
            - rs0
            - --bind_ip_all
            - --dbpath
            - /data/db
            - --auth
            - --keyFile
            - /data/key/mongodb-keyfile
            - --setParameter
            - maxTransactionLockRequestTimeoutMillis=20
          ports:
            - containerPort: 27017
          startupProbe:
            exec:
              command:
                - mongosh
                - --eval
                - "db.adminCommand('ping')"
            initialDelaySeconds: 10
            periodSeconds: 1
            timeoutSeconds: 35
            failureThreshold: 120
          livenessProbe:
            exec:
              command:
                - mongosh
                - --eval
                - "db.adminCommand('ping')"
            periodSeconds: 10
            timeoutSeconds: 35
            failureThreshold: 1
          volumeMounts:
            - name: kfc-digital-mongodb-pvc
              mountPath: /data
            - name: keyfile
              mountPath: /data/key
      imagePullSecrets:
        - name: {{ .Values.imagePullSecret.secretName }}
      terminationGracePeriodSeconds: 10
{{- include "tolerationsForNodeEviction" . | indent 6 }}
      volumes:
        - name: secret
          secret:
            secretName: kfc-digital-mongodb-keyfile-secret
            defaultMode: 0600
        - name: keyfile
          emptyDir: {}
  volumeClaimTemplates:
    - metadata:
        name: kfc-digital-mongodb-pvc
        namespace: kfc-digital-mongodb
      spec:
        resources:
          requests:
            storage: 2Gi
        accessModes:
          - ReadWriteOnce
        volumeMode: Filesystem
        {{- if eq .Values.useLonghorn true }}
        storageClassName: longhorn
        {{- else }}
        storageClassName: local-path
        {{- end}}
{{- end }}
