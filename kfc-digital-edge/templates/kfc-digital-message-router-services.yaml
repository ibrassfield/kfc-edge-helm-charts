---
{{- include "imagePullSecretTemplate" (list . "kfc-digital-message-router-services") }}

{{- $restaurantConfig := (lookup "v1" "ConfigMap" "kfc-digital-edge-common" "restaurant-config") }}
{{- $restaurantConfigData := (get $restaurantConfig "data") | default dict }}
{{- $restaurantConfigRestaurantId := (get $restaurantConfigData "RESTAURANT_ID") }}
{{- $restaurantConfigSwaggerEnabled := (get $restaurantConfigData "APP_SWAGGER_ENABLED") | quote }}
{{- $restaurantConfigTimezone := (get $restaurantConfigData "TIME_ZONE") }}
{{- $restaurantConfigRunMultiNode := (get $restaurantConfigData "RUN_MULTI_MODE") }}
{{- $restaurantConfigClusterEnvironment := (get $restaurantConfigData "CLUSTER_ENVIRONMENT") }}

{{- if ne $restaurantConfigClusterEnvironment "MOCK" }}
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: restaurant-config
  namespace: kfc-digital-message-router-services
data:
  RESTAURANT_ID: {{ $restaurantConfigRestaurantId }}
  SWAGGER_ENABLED: {{ $restaurantConfigSwaggerEnabled }}
  TIME_ZONE: {{ $restaurantConfigTimezone }}

---
apiVersion: v1
kind: Service
metadata:
  name: kfc-digital-message-router-services
  namespace: kfc-digital-message-router-services
spec:
  ports:
    - name: message-router-srv
      port: 5122
      protocol: TCP
      targetPort: 5122
  selector:
    app: kfc-digital-message-router-services
  sessionAffinity: None
  type: ClusterIP

---
apiVersion: traefik.containo.us/v1alpha1
kind: IngressRoute
metadata:
  name: servicestation
  namespace: kfc-digital-message-router-services
spec:
  entryPoints:
    - web
  routes:
    - kind: Rule
{{- if eq .Values.useIngressHostnames true }}
      match: Host(`servicestation.kfcdev.io`) && PathPrefix(`/message-router`)
{{- else }}
      match: HostRegexp(`{any:.+}`) && PathPrefix(`/message-router`)
{{- end }}
      services:
        - name: kfc-digital-message-router-services
          port: 5122
---
kind: Deployment
apiVersion: apps/v1
metadata:
  labels: 
    tags.datadoghq.com/env: {{ $restaurantConfigClusterEnvironment }}
    tags.datadoghq.com/service: {{ index "kfc-digital-message-router-services-RESTAURANTID" | replace "RESTAURANTID" $restaurantConfigRestaurantId }}
    tags.datadoghq.com/version: {{ .Chart.Version }}
  name: kfc-digital-message-router-services
  namespace: kfc-digital-message-router-services
spec:
  {{- if eq ($restaurantConfigRunMultiNode) "true" }}
  replicas: {{ .Values.messageRouterServices.multiModeReplicas }}
  {{- else }}
  replicas: 1
  {{- end }}
  selector:
    matchLabels:
      app: kfc-digital-message-router-services
  template:
    metadata:
      annotations:
        checksum/secret: {{ (include "imagePullSecretTemplate" (list . "kfc-digital-message-router-services")) | sha256sum }}
        admission.datadoghq.com/dotnet-lib.version: {{ .Values.datadog.dotnetLibVersion }}
      labels:
        app: kfc-digital-message-router-services
        admission.datadoghq.com/enabled: 'true'
        tags.datadoghq.com/env: {{ $restaurantConfigClusterEnvironment }}
        tags.datadoghq.com/service: {{ index "kfc-digital-message-router-services-RESTAURANTID" | replace "RESTAURANTID" $restaurantConfigRestaurantId }}
        tags.datadoghq.com/version: {{ .Chart.Version }}
    spec:
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
            - weight: 100
              podAffinityTerm:
                labelSelector:
                  matchExpressions:
                    - key: app
                      operator: In
                      values:
                      - kfc-digital-message-router-services
                topologyKey: kubernetes.io/hostname
      securityContext:
        seccompProfile:
          type: RuntimeDefault
      containers:
        - name: kfc-digital-message-router-services
          image: {{ .Values.messageRouterServices.image }}:{{ .Values.messageRouterServices.imageTag }}
          imagePullPolicy: IfNotPresent
          resources:
            limits:
              cpu: {{ .Values.messageRouterServices.resourcesLimitsCpu }}
              memory: {{ .Values.messageRouterServices.resourcesLimitsMemory }}
            requests:
              cpu: {{ .Values.messageRouterServices.resourcesRequestsCpu }}
              memory: {{ .Values.messageRouterServices.resourcesRequestsMemory}}
          securityContext:
            allowPrivilegeEscalation: false
            runAsNonRoot: true
            runAsUser: 10009
            runAsGroup: 10009
            capabilities:
              drop:
                - ALL
          ports:
            - containerPort: 5122
          startupProbe:
            httpGet:
              port: 5122
              path: /v1/health/readiness-check
            initialDelaySeconds: 30
            periodSeconds: 1
            failureThreshold: 150
          livenessProbe:
            httpGet:
              port: 5122
              path: /v1/health/liveness-check
            failureThreshold: 2
          envFrom:
            - secretRef:
                name: kfc-digital-message-router-services-mongodb-secret
            - secretRef:
                name: kfc-digital-message-router-services-rabbitmq-secret
          env:
            - name: ASPNETCORE_ENVIRONMENT
              value: "Production"
            - name: ASPNETCORE_URLS
              value: "http://0.0.0.0:5122"
            - name: APPLICATIONFEATURES__SWAGGERENABLED
              valueFrom:
                configMapKeyRef:
                  name: restaurant-config
                  key: SWAGGER_ENABLED
            - name: RESTAURANTCONFIGURATION__RESTAURANTID
              valueFrom:
                configMapKeyRef:
                  name: restaurant-config
                  key: RESTAURANT_ID
            - name: TZ
              valueFrom:
                configMapKeyRef:
                  name: restaurant-config
                  key: TIME_ZONE
            {{- if eq ($restaurantConfigRunMultiNode) "true"}}
            - name: MONGODBCONFIGURATION__URL
              value: "mongodb://kfc-digital-mongodb-0.kfc-digital-mongodb:27017,kfc-digital-mongodb-1.kfc-digital-mongodb:27017,kfc-digital-mongodb-2.kfc-digital-mongodb:27017/admin?w=majority&replicaSet=rs0"
            - name: RABBITMQCONFIGURATION__URLS
              value: "amqp://kfc-digital-rabbitmq-0.kfc-digital-rabbitmq,amqp://kfc-digital-rabbitmq-1.kfc-digital-rabbitmq,amqp://kfc-digital-rabbitmq-2.kfc-digital-rabbitmq"
            {{- else }}
            - name: MONGODBCONFIGURATION__URL
              value: "mongodb://kfc-digital-mongodb-0.kfc-digital-mongodb:27017/admin?w=majority"
            - name: RABBITMQCONFIGURATION__URLS
              value: "amqp://kfc-digital-rabbitmq-0.kfc-digital-rabbitmq"
            {{- end }}
            - name: DD_LOGS_INJECTION
              value: 'true'
      imagePullSecrets:
        - name: {{ .Values.imagePullSecret.secretName }}
      terminationGracePeriodSeconds: 10
{{- include "tolerationsForNodeEviction" . | indent 6 }}
{{- end }}