#!/bin/bash

if [[ -f ./localvalues.yaml ]]; then
  valueargs=" --values ./localvalues.yaml"
else
  valueargs=""  
fi

HELM_ACTION="$1"

if [ "$HELM_ACTION" = "lint" ]; then
    helm lint \
        $valueargs \
        ./kfc-digital-edge
elif [ "$HELM_ACTION" = "snyk" ]; then
    rm -rf ./kfc-digital-edge-output
    helm template ./kfc-digital-edge --output-dir ./kfc-digital-edge-output
    snyk iac test --policy-path=.snyk --severity-threshold=medium ./kfc-digital-edge-output
    rm -rf ./kfc-digital-edge-output
elif [ "$HELM_ACTION" = "snyk-all-issues" ]; then
    rm -rf ./kfc-digital-edge-output
    helm template ./kfc-digital-edge --output-dir ./kfc-digital-edge-output
    snyk iac test --policy-path=.snyk ./kfc-digital-edge-output
    rm -rf ./kfc-digital-edge-output
elif [ "$HELM_ACTION" = "dry-run" ]; then
    helm upgrade --install kfc-digital-edge \
        $valueargs \
        ./kfc-digital-edge --dry-run
elif [ "$HELM_ACTION" = "dry-run-debug" ]; then
    helm upgrade --install kfc-digital-edge \
        $valueargs \
        ./kfc-digital-edge --dry-run --debug
elif [ "$HELM_ACTION" = "upgrade" ]; then
    helm upgrade --install kfc-digital-edge \
        $valueargs \
        ./kfc-digital-edge
elif [ "$HELM_ACTION" = "delete" ]; then
    helm delete kfc-digital-edge
else
    echo "$0 [OPTION]"
    echo "* lint"
    echo "* snyk"
    echo "* snyk-all-issues"
    echo "* dry-run"
    echo "* dry-run-debug"
    echo "* upgrade"
    echo "* delete"
fi
