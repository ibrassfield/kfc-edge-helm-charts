#!/bin/bash
source .env

if [[ -f ./localvalues.yaml ]]; then
  valueargs=" --values ./localvalues.yaml"
else
  valueargs=""  
fi

HELM_ACTION="$1"

set -u # or set -o nounset
: "$HELM_IMAGE_PULL_SECRET_BASE64"
: "$RESTAURANT_CERT_PEM_BASE64"

if [ "$HELM_ACTION" = "lint" ]; then
    helm lint \
        --set restaurant.certPemBase64=$RESTAURANT_CERT_PEM_BASE64 \
        --set imagePullSecret.secretValueBase64=$HELM_IMAGE_PULL_SECRET_BASE64 \
        $valueargs \
        ./kfc-digital-edge-common
elif [ "$HELM_ACTION" = "dry-run" ]; then
    helm upgrade --install kfc-digital-edge-common \
        --set restaurant.certPemBase64=$RESTAURANT_CERT_PEM_BASE64 \
        --set imagePullSecret.secretValueBase64=$HELM_IMAGE_PULL_SECRET_BASE64 \
        $valueargs \
        ./kfc-digital-edge-common --dry-run
elif [ "$HELM_ACTION" = "dry-run-debug" ]; then
    helm upgrade --install kfc-digital-edge-common \
        --set restaurant.certPemBase64=$RESTAURANT_CERT_PEM_BASE64 \
        --set imagePullSecret.secretValueBase64=$HELM_IMAGE_PULL_SECRET_BASE64 \
        $valueargs \
        ./kfc-digital-edge-common --dry-run --debug
elif [ "$HELM_ACTION" = "upgrade" ]; then
    helm upgrade --install kfc-digital-edge-common \
        $valueargs \
        --set restaurant.certPemBase64=$RESTAURANT_CERT_PEM_BASE64 \
        --set imagePullSecret.secretValueBase64=$HELM_IMAGE_PULL_SECRET_BASE64 \
        ./kfc-digital-edge-common
elif [ "$HELM_ACTION" = "delete" ]; then
    helm delete kfc-digital-edge-common
else
    echo "$0 [OPTION]"
    echo "* lint"
    echo "* dry-run"
    echo "* dry-run-debug"
    echo "* upgrade"
    echo "* delete"
fi
